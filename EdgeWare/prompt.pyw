import sys
import os
import pathlib
import json
import random as rand
# import tkinter as tk
# from tkinter import messagebox
# from tkinter import *
# on Linux and macOS, overrideredirect prevents tkinter Text() components from receiving <KeyPress> events.
# at the time of writing, this bug has been there for more than 7 fucking years.
# i give up. qt time.
from PySide6 import QtCore, QtWidgets, QtGui
class Prompt(QtWidgets.QWidget):
    def __init__(self, text):
        super().__init__()

        self.title = QtWidgets.QLabel(command_text)
        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.title.setStyleSheet('font: 20pt')
        self.text = buildText()
        self.label = QtWidgets.QLabel(self.text)
        self.text_edit = QtWidgets.QTextEdit()
        self.button = QtWidgets.QPushButton(submission_text)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.title)
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.text_edit)
        self.layout.addWidget(self.button)

        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint) # because of course you use the OR operator for that.

        self.button.clicked.connect(self.close_if_correct)
    def destroy_and_exit(self): # my goodness, this language is shit.
        self.destroy()
        sys.exit()
    def close_if_correct(self):
        #if self.text_edit.toPlainText() == self.text:
        if checkText(self.text_edit.toPlainText(), self.text):
            self.destroy_and_exit()
    def closeEvent(self, event):
        event.ignore()

hasData = False
textData = {}
maxMistakes = 3
submission_text = 'I Submit <3'
command_text    = 'Type for me, slut~'
PATH = str(pathlib.Path(__file__).parent.absolute())
os.chdir(PATH)

with open(PATH + '/config.cfg') as settings:
    maxMistakes = int(json.loads(settings.read())['promptMistakes'])

if os.path.exists(PATH + '/resource/prompt.json'):
    hasData = True
    with open(PATH + '/resource/prompt.json', 'r') as f:
        textData = json.loads(f.read())
        try:
            submission_text = textData['subtext']
        except:
            print('no subtext')
        try:
            command_text = textData['commandtext']
        except:
            print('no commandtext')

if not hasData:
    messagebox.showerror('Prompt Error', 'Resource folder contains no "prompt.json". Either set prompt freq to 0 or add "prompt.json" to resource folder.')

def buildText():
    moodList = textData['moods']
    freqList = textData['freqList']
    outputPhraseCount = rand.randint(int(textData['minLen']), int(textData['maxLen']))
    strVar = ''
    selection = rand.choices(moodList, freqList, k=1)
    for i in range(outputPhraseCount):
        strVar += textData[selection[0]][rand.randrange(0, len(textData[selection[0]]))] + ' '
    return strVar.strip()

def checkText(a, b):
    mistakes = 0
    if len(a) != len(b):
        mistakes += abs(len(a)-len(b))
    for i in range(min(len(a), len(b))):
        if a[i] != b[i]:
            mistakes += 1
    return mistakes <= maxMistakes

try:
    app = QtWidgets.QApplication(sys.argv)
    widget = Prompt(buildText())
    screen = app.primaryScreen()
    w = screen.size().width()
    h = screen.size().height()
    widget.setGeometry(rand.randint(0, w), rand.randint(0, h), w/6, h/3)
    widget.resize(800, 600)
    widget.show()
    sys.exit(app.exec())
except Exception as e:
    messagebox.showerror('Prompt Error', 'Could not create prompt window.\n[' + str(e) + ']')
