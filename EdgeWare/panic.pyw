import ctypes
import os
import subprocess
import pathlib
PATH = str(pathlib.Path(__file__).parent.absolute())

timeObjPath = os.path.join(PATH, 'hid_time.dat')
HIDDEN_ATTR = 0x02
SHOWN_ATTR  = 0x08

if os.path.exists(os.path.join(PATH, 'hid_time.dat')):
    #ctypes.windll.kernel32.SetFileAttributesW(timeObjPath, HIDDEN_ATTR)
    #sudoku if timer after hiding file again
    os.kill(os.getpid(), 9)

subprocess.call(['pkill', '-e', 'python'])
