# Edgeware for Linux
This is a clone of https://github.com/PetitTournesol/Edgeware adapted for Linux systems.
**DISCLAIMERS:** 
- I, JBerliner, have zero experience with Python. Any issues related to the primary functionality of this software are PetitTournesol's fault, and JBerliner probably can't fix them.
- This is not a full description of the software. This file will primarily focus on the differences between the original software and my adaptation. You might want to consult the original repository's readme for features and stuff.


# Edgeware
**First and foremost as a disclaimer: this is NOT actually malicious software. It is intended for entertainment purposes only. Any and all damage caused to your files or computer is _YOUR_ responsibility. If you're worried about losing things, BACK THEM UP.**
For a more detailed description of Edgeware and how to use it, please consult https://github.com/PetitTournesol/Edgeware/README.md

## Why It Doesn't "Just Work" On Linux
1. Someone (PetitTournesol) decided to use Windows path separators (`\`) instead of UNIX path separators (`/`). This is a really bad decision which has probably caused PetitTournesol numerous headaches when dealing with path expansions. The original code contains multiple instances of `str.replace('\\', '\\\\')`.
2. Some Python packages don't behave the same on Windows and on Linux:
    - `os.startfile` doesn't exist
    - `ctypes.windll` doesn't exist (obviously)
    - `tkinter` is generally quite outdated, and has a serious (for this application) problem on Linux: `overrideredirect` (the thing that makes windows unclosable by conventional means) prevents windows from receiving KeyPress events. So, **Edgeware is being rewritten with PyQt instead of tkinter.** You can access this version on the `pyqt-rewrite` branch. It's not very good though.
3. Linux users have not heard of malware (/j). Linux is a paradise where viruses don't exist (/srs).
4. Some parts of it barely work on Windows anyway, because... Windows.
5. Startup works differently. This should never have been a built-in option. Whatever, I'm writing a systemd service.

## Known Bugs I (JBerliner) Might Try My Hand At
- *"THE BOORU DOWNLOADER IS OUTDATED AND BROKEN. IT WILL LIKE BARELY FUNCTION, IF AT ALL. No I will not fix it, this shit is a pain in the ass and I'm stupid."* - PetitTournesol (but JBerliner might try fixing it)
- the panic shortcut doesn't work due to tkinter issues
- wallpaper changing feature removed. pretty much every DE/WM has its own way to set wallpapers, and there is no single universal wallpaper changing script. **[I have already dealt with this once,](https://gitlab.com/JBerliner/walltaker-client)** and do not want to do so again.

if you notice one not mentioned here, inform JBerliner and/or PetitTournesol and it **might** be fixed. (PetitTournesol has not explicitly consented to being bothered about this, so if you're annoying that's your own fault.)
## Tested Features
(tested on GNOME)
- Popups
- Prompts
- Mitosis
- Subliminals
- Denial Mode
- Drive filling
- Image replacing
- Lowkey Mode
- Hibernate Mode

## Setup
```
git clone https://gitlab.com/JBerliner/Edgeware
cd Edgeware
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install -r pip-packages.txt
```
Set up autostart via the provided systemd service (optional):
```
cd Edgeware/EdgeWare
cp edgeware.service.dist edgeware.service
sed -ie "s|start.pyw|$PWD/start.pyw|" edgeware.service
ln -s $(realpath edgeware.service) ~/.config/systemd/user/edgeware.service

```
`sed` is the best command line utility ever created.
## Configuration
```
python config.pyw
```
Note: The "Launch on Startup" option has been removed from the program itself and replaced by a systemd service.
Enable launching on startup with `systemctl --user enable edgeware.service` and disable it with `systemctl --user disable edgeware.service`
## Extracting Assets
Extract the Edgeware pack you wish to use into `EdgeWare/resource`. You can find packs at the bottom of this page or in [this MEGA folder](https://mega.nz/folder/OZUQ2KRa#wxRdlf5Q5wv_zKHvq83Sxg).
## Launching
```
python start.pyw
```
or via the provided systemd service
## Exiting
- the "Panic" option from the system tray menu
- launch Edgeware from a fullscreen terminal so you can spam Ctrl+C in it
- switch to a TTY with Ctrl+Alt+F[whatever], logging in and `pkill -ef python`
- make a shortcut for `pkill -ef python` and use that as a panic button

# Premade Packages

**Blacked**

*Standard Blacked hentai stuff, includes Porn Addict Brainwash Program: BBC Edition, and some volafile mp3s from /trash/ Blacked threads*

https://drive.google.com/file/d/1BHLrCO5cvm9YCF_EeWGYS8AmAsPxUZPJ/view?usp=sharing

https://mega.nz/file/IfJS1JLB#eEkreHNBH5g_maKsiUC0I1BOeh1FvdOwU5i-Eto6FwA

**Gay Yiff**

*Includes lots and lots of steamy, hulking furry cocks*

https://drive.google.com/file/d/1b2gOJBLy-nD5p1cOM8xTDPh7LGsf1g58/view?usp=sharing

https://mega.nz/file/kOA2DZAJ#5A7pfQUdEKq8s3ner4dhmrKxS7xoYupMcNAAK3voU3M

**Censored**

*For the people who get off to not getting off*

https://drive.google.com/file/d/1phBN4JhoyOg3yAMomGgIKVTryYc8dv4Q/view?usp=sharing

https://mega.nz/file/lPBUxRyA#AJlC4Kwrtdci3cjISWkZ8YThuWEmyHcL81MfZoprrqQ

**Hypno**

*Includes the most gifs of any pack by far, as well as Porn Addict Brainwash Program 5 & 6, and Queue Balls 1*

https://drive.google.com/file/d/1W2u_wAp2DAWa-h0O5VUlGKhKSVMwQkh5/view?usp=sharing

https://mega.nz/file/YHB2ABAa#QApGJHeg6EF-20VP0OVf8yZyQtmCdZRQduXrHOHvUCM

**Hentai/Basic Gooner**

*Includes mostly 3D or hentai images, has 100% gifs, some videos, and audio from the porn addict brainwash program and queue balls*

https://drive.google.com/file/d/10_t11qm_2fRp4GVh0JK4hskdwW9ppCme/view?usp=sharing

https://mega.nz/file/gWwDmYJT#xWGsdfaPB5TvsnvDC4OUEWR06flqn7Bc9pvOErSUBuY

**No Limit Gooner**

*Includes the same audio as Hypno, as well as an MLP worship themed hypnosis file by https://twitter.com/AlmondMilkomg. Heavier focus on stranger kinks such as ponies, furry, farts, cringe, emoji, etc.*

https://drive.google.com/file/d/1nExnM00ODbZjAV2w8UX-Ybw8wp3ffNjK/view?usp=sharing

https://mega.nz/file/0SQEzZrb#UK6SSDUFz8u_xM5lcNMStqQdS-bqE_ilB6u7RkGUjGM

**Elsa**

*More or less all of the original assets and resources from the original Elsavirus. It's not a 1:1 experience, but if you liked the Elsa theming and original writing, it's all still there.*

*Warning that this pack does contain ALL of the resources from the original Elsavirus, including the noose image.*

https://drive.google.com/file/d/1QLJI52zM9HrJP_ozNxLaUSSEVUoDpjJP/view?usp=sharing

https://mega.nz/file/JKRCHR4a#SnxrAar_rvhK4BYjewz1TZmtV6EEeyEG9QU8JTkWOck

**Futanari**

*Futa themed pack, includes some generic moaning and plap plap audio. This and the Elsa pack are the first to make use of the caption feature, older packs will be updated with new resources and caption assets in the coming weeks.*

https://drive.google.com/file/d/12L9lKiOzKgBlDaoPudNhkYgZH-3khUeh/view?usp=sharing

https://mega.nz/file/AWQUTDYb#1rjRbHbDfqTVt-w7m-IryWFAf95us0tg3kBq-5VybGw


__**FAQ**__

**Q: "Why do I keep getting white circles in my popups?"**

**A: *This occurs when the resource folder is generated without any resource zip in the script folder. Either delete your resource folder and restart Edgeware with the zip located properly or manually import your zip with the config function.***

**Q: "Where does the booru downloader save files?"**

**A: *The booru downloader saves all files it downloads into the /resource/img/ folder.***


__**What is Edgeware?**__

Edgeware is an Elsavirus inspired fetishware tool, built from the ground up to use interchangeable resource packages for easily customized user experience.

Much like Elsavirus and Doppelvirus, this program was written in brainlet level Python, but unlike the two of them, has no compiled executables. If you're the type to fear some hidden actually malicious scripts, this ensures that *all* of the code is front and center; no C++/C# forms or other tricks that might hide the true nature of the application.


The software features the popups, hard drive filling, porn library replacing, website opening features of its predecesors.

Edgeware *does* include some unique features to make it more widely applicable than just the previous respective target demographics of /beta/ participants and finsub followers. Namely its packaging system, which allows anyone to cater the experience to their own particular interests or fetishes. Either place a properly assembled zip file named "resources.zip" in the same folder as the scripts so that the program can unpack it or manually extract the resources folder into the said directory.

I more or less went into this wanting to make my own version of Elsavirus/Doppelvirus for fun, but figured around halfway that it might be worthwhile to share it with others who might have similar tastes.

Obviously you need to have Python installed, but other than that there should be no dependencies that aren't natively packaged with the language itself.

__**Packages**__

  Packages must be structured as follows:
  
    (name).zip
       ->aud
         (Audio Files) (Optional)
       ->img
         (Image Files, Gif Files)
	   ->subliminals
	     (Gif files only) (Optional)
       ->vid
         (Video Files) (Optional)
       icon.ico
       wallpaper.png
       web.json (Optional)
       prompt.json (Optional)
	   discord.dat (Optional)
	   captions.json (Optional)
   
  The web.json file should contain two sets:
  
    {"urls":["url1", "url2", ...], "args":["arg1,arg2,arg3", "", "arg1,arg2", ...]}
    ->urls - set of urls
    ->args - corresponding set of arguments; even if a url should take no argument, there must be a "" in this
      ->args are separated by commas within their strings, eg "arg1,arg2,arg3"
      ->ensure that urls and args are aligned; if the first URL can take the args "a,b" the first args value should be "a,b"
      ->args will be selected randomly and appended to the end of the url
        ->eg, "https://www.google.com/" with args "penis,cock,ass" cound randomly return one of 
        ->https://www.google.com/penis  https://www.google.com/cock  https://www.google.com/ass
        
  The prompt.json file should contain any number of sets:
  
    {"moods":["mood1", "mood2", "angryMood"], "freqList":[10, 40, 50], "minLen":2, "maxLen"=4, "mood1":["mood1 sentence 1.", "mood1 sentence 2."], "mood2":["mood2 only has 1 sentence."], "angryMood":["angryMood also has one sentence."]}
        ->moods - names don't matter, as long as they're accounted for later in the set.
        ->freqList - correspond to each value in moods, define the frequency of that mood being selected.
        ->min/maxLen - minimum number of sentences that can be selected vs maximum.
        ->mood name
            ->can contain any number of mood related sentences.
            ->will ONLY select from this set if that mood is selected.
            
If resources are present, but not properly structured, the application could crash or exhibit strange behavior.
